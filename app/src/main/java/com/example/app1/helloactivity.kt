package com.example.app1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.app1.databinding.ActivityHelloactivityBinding
import com.example.app1.databinding.ActivityMainBinding

class helloactivity : AppCompatActivity() {
    private lateinit var binding: ActivityHelloactivityBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding =ActivityHelloactivityBinding.inflate(this.layoutInflater)
        val view = binding.root
        setContentView(view)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        val name = intent?.extras?.getSerializable("name").toString()
        binding.nameactivity.text = name
    }  override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }


}