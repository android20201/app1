package com.example.app1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import com.example.app1.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var listIntent: Intent

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(R.layout.activity_main)
        setContentView(binding.root)
        // Launch the VerticalListActivity on verticalBtn click
        binding.button.setOnClickListener { launchHello() }
        findViewById<Button>(R.id.button).setOnClickListener{
            var intent =Intent(this,helloactivity::class.java)
            var name = binding.name.text.toString()
            Log.d("Showname",name)
            intent.putExtra("name",name)
            this.startActivity(intent)
        }


    }

    private fun launchHello() {
        listIntent = Intent(this, helloactivity::class.java)
        startActivity(listIntent)
    }
    }
